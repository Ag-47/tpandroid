package com.epsi.adriangandon.tpandroid2;

import android.app.LauncherActivity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class DeuxiemePageTP extends AppCompatActivity {
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deuxieme_page_tp);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = (ListView) findViewById(R.id.listView1);

        String[] names = new String[] { "Necron1", "Necron2", "Necron3", "Necron4" };
        int[] ages = { 66, 66, 66, 66 };
        int[] images = { R.drawable.necron, R.drawable.necron, R.drawable.necron, R.drawable.necron };

        ArrayList<ListItem> myList = new ArrayList<ListItem>();

        for (int i = 0; i < names.length; i++){
            myList.add(new ListItem(names[i], ages[i], images[i]));
        }

        CustomAdapter adapter = new CustomAdapter(this, myList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(adapter);
    }

}
