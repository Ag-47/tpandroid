package com.epsi.adriangandon.tpandroid2;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class TroisiemePageTP extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_troisieme_page_tp);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ImageView uneImage = (ImageView) findViewById(R.id.imageView);

        Picasso.with(uneImage.getContext()).load(R.drawable.necron2).into(uneImage);
        // --> Necessite internet pour celle-ci :
        //Picasso.with(uneImage.getContext()).load("http://pro.bols.netdna-cdn.com/wp-content/uploads/2015/01/IMG_0342.480x480-75.jpg").into(uneImage);
    }

}
