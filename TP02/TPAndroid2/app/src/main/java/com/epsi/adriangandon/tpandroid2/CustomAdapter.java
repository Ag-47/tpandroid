package com.epsi.adriangandon.tpandroid2;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.R.layout;

import com.epsi.adriangandon.tpandroid2.DeuxiemePageTP;

import java.util.ArrayList;

/**
 * Created by Adrian Gandon on 12/10/2016.
 */

public class CustomAdapter extends BaseAdapter implements AdapterView.OnItemClickListener{

    private ArrayList<ListItem> myList = new ArrayList<>();
    private Context context;

    // On passe le context afin d'obtenir un LayoutInfFlater pour utiliser notre row_layout.xml
    // On passe les valeurs de notre à l'adapter
    public CustomAdapter(Context context, ArrayList<ListItem> myList){
        this.myList = myList;
        this.context = context;
    }

    // Retourne le nombre d'objet présent dans notre liste
    @Override
    public int getCount(){
        return  myList.size();
    }

    // Retourne un élément de notre liste en fonction de sa position
    @Override
    public ListItem getItem(int position){
        return  myList.get(position);
    }

    // Retourne l'id d'un élément de notre liste en fonction de sa position
    @Override
    public long getItemId(int position){
        return myList.indexOf(getItem(position));
    }

    // Retourne la vue d'un élément de la liste
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        MyViewHolder mViewHolder = null;

        // Au premier appel ConvertView est null, on inflate notre layout
        if(convertView == null){
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            convertView = mInflater.inflate(R.layout.activity_deuxieme_page_tp, parent, false);

            // Nous plaçons dans notre MyViewHolder les vues de notre layout
            mViewHolder = new MyViewHolder();
            mViewHolder.textViewName = (TextView) convertView.findViewById(R.id.textViewName);
            mViewHolder.textViewAge = (TextView) convertView.findViewById(R.id.textViewAge);
            mViewHolder.imageView = (ImageView) convertView.findViewById(R.id.imageView);

            // Nous attribuons comme tag notre MyViewHolder à convertView
            convertView.setTag(mViewHolder);
        } else {
            // ConvertView n'est pas null, nous récupérons notre objet MyViewHolder
            // Et évitons ainsi de devoir retrouver les vues à chaque appel de getView
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        // Nous récupérons l'item de la liste demandé par getView
        ListItem listItem = (ListItem) getItem(position);

        // Nous pouvons  attribuer à nos vues les valeurs de l'élément de la liste
        mViewHolder.textViewName.setText(listItem.getName());
        mViewHolder.textViewAge.setText(String.valueOf(listItem.getAge()) + " ans");
        mViewHolder.imageView.setImageResource(listItem.getImageId());

        // Nous retournons la vue de l'item demandé
        return convertView;
    }

    // MyViewHolder va nous permettre de ne pas devoir rechercher
    // Les vues à chaque appel de getView, nous gagnons ainsi en performance
    private  class MyViewHolder{
        TextView textViewName, textViewAge;
        ImageView imageView;
    }

    // Nous affichons un Toast à chaque clic sur un item de la liste
    // Nous récupérons l'objet grâce à sa position
    @Override
    public  void onItemClick(AdapterView<?> parent, View view, int position, long id){
        Toast toast = Toast.makeText(context, "Item " + (position + 1) + ": " + this.myList.get(position), Toast.LENGTH_SHORT);
        toast.show();
    }
}
