package com.epsi.adriangandon.tpandroid2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class PremierePageTP extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premiere_page_tp);
        setTitle("Premiere Page");

        Button button = (Button)findViewById(R.id.autreMethode);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PremierePageTP.this, DeuxiemePageTP.class));
            }
        });

        Button button2 = (Button)findViewById(R.id.autreMethod2);
        button2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                startActivity(new Intent(PremierePageTP.this, PicassoPage.class));
            }
        });

        ListView uneListeView = (ListView)findViewById(R.id.listView1);

        String[][] infoNecron = new String[][]{
                {"Necron1", "66 millions d'annees"},
                {"Necron2", "66 millions d'annees"},
                {"Necron3", "66 millions d'annees"},
                {"Necron4", "66 millions d'annees"},
                {"Necron5", "66 millions d'annees"},
                {"Necron6", "66 millions d'annees"}};

        List<HashMap<String, String>> uneSuperListeDeSupersNecrons = new ArrayList<HashMap<String, String>>();
        HashMap<String, String>  unElement;

        for (int i = 0; i < infoNecron.length; i++)
        {
            unElement = new HashMap<String, String>();
            unElement.put("text1", infoNecron[i][0]);
            unElement.put("text2", infoNecron[i][1]);

            uneSuperListeDeSupersNecrons.add(unElement);
        }

        ListAdapter unAdapterDeListeDeNecron = new SimpleAdapter(this, uneSuperListeDeSupersNecrons, android.R.layout.simple_expandable_list_item_2,
                new String[] {"text1", "text2"}, new int[]{ android.R.id.text1, android.R.id.text2});

        uneListeView.setAdapter(unAdapterDeListeDeNecron);


        // Instanciation d'un tableau
        /*String[] nomNecron = new String[] { "Necron1", "Necron2", "Necron3", "Necron4", "Necron5", "Necron6"};

        String[] ageNecron = new String[] { "66m", "66m", "66m", "66m", "66m", "66m" };

        // Instanciation d'une liste (l'item liste)
        ListView uneListeView = (ListView)findViewById(R.id.listView1);

        // Instanciation d'un ArrayAdapter (qui va permettre par la suite d'afficher un tableau dans une ListView
        ArrayAdapter<String> necronAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, nomNecron);
        //ArrayAdapter<String> ageNecronAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, ageNecron); */

        //List<Map<String, String>> uneListeDItem = new ArrayList<Map<String, String>>(6);
        //Map<String, String> uneMegaTropBienListe = new HashMap<String, String>();


        //uneListeDItem.add(uneMegaTropBienListe);

        //uneListeView.setAdapter(uneMegaTropBienListe);

        // Permet l'affichage du tableau
        //uneListeView.setAdapter(necronAdapter, ageNecronAdapter);
    }
}
