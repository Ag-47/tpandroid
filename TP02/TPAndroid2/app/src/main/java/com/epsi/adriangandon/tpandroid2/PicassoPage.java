package com.epsi.adriangandon.tpandroid2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class PicassoPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picasso_page);

        ImageView imageView;
        imageView = (ImageView) findViewById(R.id.imageView);

        //Picasso.with(imageView.getContext()).load(R.drawable.necron2).into(imageView);
        Picasso.with(imageView.getContext()).load("http://pro.bols.netdna-cdn.com/wp-content/uploads/2015/01/IMG_0342.480x480-75.jpg").into(imageView);
    }
}
