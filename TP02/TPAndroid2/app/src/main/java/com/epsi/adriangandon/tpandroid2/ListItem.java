package com.epsi.adriangandon.tpandroid2;

/**
 * Created by Adrian Gandon on 12/10/2016.
 */

public class ListItem {
    private String name;
    private  int imageId;
    private  int age;

    public ListItem(String name, int age, int imageId){
        this.setAge(age);
        this.setImageId(imageId);
        this.setName(name);
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public int getAge(){
        return age;
    }
    public void setAge(int age){
        this.age = age;
    }
    public int getImageId(){
        return imageId;
    }
    public void setImageId(int imageId){
        this.imageId = imageId;
    }

    @Override
    public String toString(){
        return this.name + " : " + this.age;
    }
}
