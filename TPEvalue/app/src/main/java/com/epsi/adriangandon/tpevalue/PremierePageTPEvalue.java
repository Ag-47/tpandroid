package com.epsi.adriangandon.tpevalue;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PremierePageTPEvalue extends AppCompatActivity {
    DataBaseFormulaire myDb;
    EditText txtNom, txtPrenom, txtAge, txtVille, txtPays;
    Button btnValider;
    Button btnInfoClient;
    Button btnSupprimerBaseDeDonnee;
    Button btnVersModif;
    Button btnVersSuppression;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premiere_page_tpevalue);
        setTitle("Ajouter un client");
        myDb = new DataBaseFormulaire(this);

        txtNom = (EditText) findViewById(R.id.txtNom);
        txtPrenom = (EditText) findViewById(R.id.txtPrenom);
        txtAge = (EditText) findViewById(R.id.txtAge);
        txtVille = (EditText) findViewById(R.id.txtVille);
        txtPays = (EditText) findViewById(R.id.txtPays);

        btnValider = (Button) findViewById(R.id.btnValider);
        btnInfoClient = (Button) findViewById(R.id.btnConsulterInfo);
        btnSupprimerBaseDeDonnee = (Button) findViewById(R.id.btnSupprDB);
        btnVersModif = (Button) findViewById(R.id.btnVersModificationClient);
        btnVersSuppression = (Button) findViewById(R.id.btnVersPageSuppr);

        AjouterDonnee();
        consulterToutesLesInfosSurLesClient();
        SupprimerBDD();
        VersModifDonnees();
        versSuppressionClient();
    }

    public void AjouterDonnee(){
        btnValider.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View uneVue){
                        boolean isInserted = myDb.insertData(txtNom.getText().toString(), txtPrenom.getText().toString(),
                                txtAge.getText().toString(), txtVille.getText().toString(),
                                txtPays.getText().toString());
                        if(isInserted == true)
                            Toast.makeText(PremierePageTPEvalue.this, "Les données ont correctement été insérées.", Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(PremierePageTPEvalue.this, "Les données n'ont pas été correctement insérées.", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    public void SupprimerBDD(){
        btnSupprimerBaseDeDonnee.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getBaseContext().deleteDatabase(myDb.getDatabaseName());
                        Toast.makeText(PremierePageTPEvalue.this, "Données correctement réinitialisées, veuillez relancer l'application pour que les modifications soient effectives.", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    public void VersModifDonnees(){
        btnVersModif.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(PremierePageTPEvalue.this, DeuxiemePageTPEvaluee.class));
                    }
                }
        );
    }

    public void versSuppressionClient(){
        btnVersSuppression.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(PremierePageTPEvalue.this, TroisiemePageTPEvaluee.class));
                    }
                }
        );
    }

    public void consulterToutesLesInfosSurLesClient(){
        btnInfoClient.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Cursor res = myDb.getAllData();
                        if(res.getCount() == 0){
                            showMessage("Aucun client", "Rien n'a été trouvé.");
                            return;
                        }

                        StringBuffer buffer = new StringBuffer();
                        while(res.moveToNext()){
                            buffer.append("Id : " + res.getString(0) + "\n");
                            buffer.append("Nom : " + res.getString(1) + "\n");
                            buffer.append("Prénom : " + res.getString(2) + "\n");
                            buffer.append("Age : " + res.getString(3) + "\n");
                            buffer.append("Ville : " + res.getString(4) + "\n");
                            buffer.append("Pays : " + res.getString(5) + "\n\n");
                        }
                        showMessage("Clients : ", buffer.toString());
                    }
                }
        );
    }

    public void showMessage(String title, String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }
}
