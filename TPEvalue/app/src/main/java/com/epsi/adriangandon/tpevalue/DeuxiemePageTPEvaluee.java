package com.epsi.adriangandon.tpevalue;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DeuxiemePageTPEvaluee extends AppCompatActivity {
    Button btnValiderModification, btnVoirClient;
    EditText txtNom, txtPrenom, txtAge, txtVille, txtPays, txtID;
    DataBaseFormulaire myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deuxieme_page_tpevaluee);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnValiderModification = (Button) findViewById(R.id.btnValiderModif);
        btnVoirClient = (Button) findViewById(R.id.btnVoirClient);

        txtPrenom = (EditText) findViewById(R.id.txtModPrenom);
        txtNom = (EditText) findViewById(R.id.txtModNom);
        txtAge = (EditText) findViewById(R.id.txtModAge);
        txtVille = (EditText) findViewById(R.id.txtModVille);
        txtPays = (EditText) findViewById(R.id.txtModPays);
        txtID = (EditText) findViewById(R.id.txtModID);

        myDb = new DataBaseFormulaire(this);

        mettreAjourDonneClient();
        consulterToutesLesInfosSurLesClient();
    }

    public void mettreAjourDonneClient(){
        btnValiderModification.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try
                        {
                            if(txtPays.getText() != null && txtID.getText() != null && txtPrenom.getText() != null && txtNom.getText() != null &&
                                    txtVille.getText() != null && txtAge.getText() != null &&
                                    txtPays.getText().toString() != " " && txtID.getText().toString() != " " && txtPrenom.getText().toString() != " " &&
                                    txtNom.getText().toString() != " " && txtVille.getText().toString() != " " && txtAge.getText().toString() != " ")
                            {
                                boolean isUpdated = myDb.updateData(txtID.getText().toString(), txtNom.getText().toString(),
                                        txtPrenom.getText().toString(), txtAge.getText().toString(), txtVille.getText().toString(), txtPays.getText().toString());
                                if(isUpdated)
                                    Toast.makeText(DeuxiemePageTPEvaluee.this, "Les données ont été correctement modifiées.", Toast.LENGTH_LONG).show();
                                else
                                    Toast.makeText(DeuxiemePageTPEvaluee.this, "Les données n'ont pas été correctement modifiées.", Toast.LENGTH_LONG).show();
                            }
                            else
                                Toast.makeText(DeuxiemePageTPEvaluee.this, "Les données entrées ne peuvent être nulle.", Toast.LENGTH_LONG).show();
                        }
                        catch (Exception e)
                        {
                            Toast.makeText(DeuxiemePageTPEvaluee.this, e.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                }
        );
    }
    public void consulterToutesLesInfosSurLesClient(){
        btnVoirClient.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Cursor res = myDb.getAllData();
                        if(res.getCount() == 0){
                            showMessage("Aucun client", "Rien n'a été trouvé.");
                            return;
                        }

                        StringBuffer buffer = new StringBuffer();
                        while(res.moveToNext()){
                            buffer.append("Id : " + res.getString(0) + "\n");
                            buffer.append("Nom : " + res.getString(1) + "\n");
                            buffer.append("Prénom : " + res.getString(2) + "\n");
                            buffer.append("Age : " + res.getString(3) + "\n");
                            buffer.append("Ville : " + res.getString(4) + "\n");
                            buffer.append("Pays : " + res.getString(5) + "\n\n");
                        }
                        showMessage("Clients : ", buffer.toString());
                    }
                }
        );
    }
    public void showMessage(String title, String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }
}
