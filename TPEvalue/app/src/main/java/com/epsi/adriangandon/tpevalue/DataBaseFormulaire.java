package com.epsi.adriangandon.tpevalue;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Adrian Gandon on 13/10/2016.
 */

public class DataBaseFormulaire extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "TPEvalue.db";
    public static final String TABLE_NAME = "personne_table";
    public static final String COL_1 = "ID";
    public static final String COL_2 = "NOM";
    public static final String COL_3 = "PRENOM";
    public static final String COL_4 = "AGE";
    public static final String COL_5 = "VILLE";
    public static final String COL_6 = "PAYS";

    public DataBaseFormulaire(Context context) {
        super(context, DATABASE_NAME, null, 1);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, NOM TEXT, PRENOM TEXT, AGE TEXT, VILLE TEXT, PAYS TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public Boolean insertData(String nom, String prenom, String age, String ville, String pays){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, nom);
        contentValues.put(COL_3, prenom);
        contentValues.put(COL_4, age);
        contentValues.put(COL_5, ville);
        contentValues.put(COL_6, pays);
         long result = db.insert(TABLE_NAME, null, contentValues);
        if(result == -1)
            return  false;
        else
            return true;
    }

    public Cursor getAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        return  res;
    }

    public boolean updateData(String id, String nom, String prenom, String age, String ville, String pays){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1, id);
        contentValues.put(COL_2, nom);
        contentValues.put(COL_3, prenom);
        contentValues.put(COL_4, age);
        contentValues.put(COL_5, ville);
        contentValues.put(COL_6, pays);
        db.update(TABLE_NAME, contentValues, "ID = ?", new String[] { id });

        return  true;
    }

    public  Integer deleteData(String ID){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "ID = ?", new  String[] { ID });
    }
}
