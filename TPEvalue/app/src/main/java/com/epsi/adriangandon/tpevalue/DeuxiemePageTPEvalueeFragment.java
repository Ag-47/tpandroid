package com.epsi.adriangandon.tpevalue;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * A placeholder fragment containing a simple view.
 */
public class DeuxiemePageTPEvalueeFragment extends Fragment {

    public DeuxiemePageTPEvalueeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_deuxieme_page_tpevaluee, container, false);
    }
}
