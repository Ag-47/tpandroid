package com.epsi.adriangandon.tpevalue;

import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class TroisiemePageTPEvaluee extends AppCompatActivity {
    Button btnVoirTousLesClientsExistantsAvantSuppression, btnSupprimerLeClient;
    DataBaseFormulaire myDb;
    EditText txtID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_troisieme_page_tpevaluee);
        setTitle("Supprimer un client");

        btnVoirTousLesClientsExistantsAvantSuppression = (Button) findViewById(R.id.btnVoirClientAvantSuppr);
        btnSupprimerLeClient = (Button) findViewById(R.id.btnSupprimerClient);

        myDb = new DataBaseFormulaire(this);
        txtID = (EditText) findViewById(R.id.txtAvantSupprID);

        consulterToutesLesInfosSurLesClient();
        deleteData();
    }

    public void deleteData(){
        btnSupprimerLeClient.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Integer deletedRows = myDb.deleteData(txtID.getText().toString());
                        if(deletedRows > 0){
                            Toast.makeText(TroisiemePageTPEvaluee.this, "Client correctement supprimée.", Toast.LENGTH_LONG).show();
                        }
                        else Toast.makeText(TroisiemePageTPEvaluee.this, "Client non supprimée.", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    public void consulterToutesLesInfosSurLesClient(){
        btnVoirTousLesClientsExistantsAvantSuppression.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Cursor res = myDb.getAllData();
                        if(res.getCount() == 0){
                            showMessage("Aucun Client", "Rien n'a été trouvé.");
                            return;
                        }

                        StringBuffer buffer = new StringBuffer();
                        while(res.moveToNext()){
                            buffer.append("Id : " + res.getString(0) + "\n");
                            buffer.append("Nom : " + res.getString(1) + "\n");
                            buffer.append("Prénom : " + res.getString(2) + "\n");
                            buffer.append("Age : " + res.getString(3) + "\n");
                            buffer.append("Ville : " + res.getString(4) + "\n");
                            buffer.append("Pays : " + res.getString(5) + "\n\n");
                        }
                        showMessage("Clients : ", buffer.toString());
                    }
                }
        );
    }
    public void showMessage(String title, String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }
}
